#!/usr/bin/env python
"""
IRC Bot for registering IRC users with mumble's authentication backend
"""
from os import getenv
from sys import exit
import socket
import ssl
# import time
# import re
import string
import random
import urllib.request
import hashlib

def randompassword():
  chars = string.ascii_uppercase + string.ascii_lowercase + string.digits
  size = random.randint(8, 12)
  return ''.join(random.choice(chars) for x in range(size))

# irc server config
host="irc.darkscience.net"
port = 6697
nick = "mumblebot"
channel = "#voicechat"

try:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
except socket.error as msg:
    print('Failed to create socket. Error code: ' + str(msg[0]) + ' , Error message : ' + msg[1])
    exit(1)

try:
    s.connect((host, port))
except socket.error:
    print('Failed to connect to irc server')
    exit(1)

#warning: only use for testing to allow for self signed certs. This will disable SSL verification
#ctx = ssl.create_default_context()
#ctx.check_hostname = False
#ctx.verify_mode = ssl.CERT_NONE

irc = ssl.wrap_socket(s)
#irc.send(bytes("PASS " + password + "\n", 'utf-8'))
irc.send(bytes("USER {0} {0} {0} :Mumble Bot\n".format(nick), 'utf-8'))
irc.send(bytes("NICK " + nick + "\n", 'utf-8'))
masterpassword = getenv('BOT_PASSWORD', "thisneedstobelongandrandom")
mumbleserveraddress = getenv('MUMBLE_SERVER', 'domain.com')
registationlocation = "/registration"
registeringusers = []
joined = False
print("Connected to IRC server")
while True:
    data = irc.recv(1024).decode("utf-8")
    if not data:
        break

    text = data.decode("utf-8")
    print(text)

    if text.find('PING') != -1:
        sendstring = 'PONG ' + text.split()[1] + '\r\n'
        irc.send(bytes(sendstring, 'utf-8'))
    if text.find(':NickServ!NickServ@services.darkscience.net') != -1 \
            and joined is False:
        irc.send(bytes("MODE +B +" + nick, 'utf-8'))  # DS requires this
        irc.send(bytes("JOIN " + channel + "\r\n", 'utf-8'))
        joined = True
    if text.find('PRIVMSG #voicechat :!register') != -1:
        split_text = text.split("!")
        nick = split_text[0].strip(':')
        irc.send(bytes("PRIVMSG NickServ :ACC " + nick + "\r\n", 'utf-8'))
        registeringusers.append(nick)

    for user in registeringusers:
        if text.find(":NickServ!NickServ@services.darkscience.net NOTICE " + nick + " :" + user + " ACC") != -1:
            accstring = text.split(":")[2].strip()
            if accstring == (user + " ACC 3"):
                userpassword = randompassword()
                fp = urllib.request.urlopen("https://" + mumbleserveraddress + registationlocation + "?username=" + user + "&password="+hashlib.sha1(userpassword.encode()).hexdigest() + "&serverpassd=" + masterpassword)
                mybytes = fp.read()

                response = mybytes.decode("utf8")
                fp.close()

                if response == "User Registered":
                    irc.send(bytes("PRIVMSG " + user + " :User registered! Connect with standalone mumble client with Server: " + mumbleserveraddress + ", Username: " + user + ", Password: "+ userpassword + " Or connect with web mumble client (username and password autofilled): https://" + mumbleserveraddress + "/?username="+nick + "&password="+userpassword+"\r\n", 'utf-8'))
                else:
                    irc.send(bytes("PRIVMSG " + user + " :Error Adding User\r\n", 'utf-8'))
            else:
                irc.send(bytes("PRIVMSG " + user + " :You are not logged in\r\n", 'utf-8'))
            del registeringusers[registeringusers.index(user)]
